/// <reference types="cypress" />

describe('Self Sign', () => {
    it('sign', () => {
        cy.visit('https://privy.id/')
        cy.contains('Login').click({
            force: true
        })
        cy.contains('Log In').should('exist')
        cy.get('input[name="user[privyId]"]').type('CG8758', {
            force: true
        })
        cy.contains('CONTINUE').click()
        cy.get('input[name="user[secret]"]').type('Prayoga123', {
            force: true
        })
        cy.contains('CONTINUE').click()
        cy.get('#v-step-0').click()
        cy.contains('Self Sign').click()
        cy.contains('Drag your document here or click browse').click()

        const fileName = 'Doc1.pdf';

        cy.get('[type="file"]').attachFile({
            filePath: fileName,
            encoding: 'base64'
        })
        cy.get('.modal-content .modal-footer button:contains("Upload")').click()
        cy.wait(1000)
        cy.contains('Continue').click()
        cy.wait(1000)
        cy.get('#step-document-1').click()
        cy.wait(1000)
        cy.contains('Done').click()
        cy.wait(2000)
        cy.contains('Send via QR Code').click()
        cy.contains('Send OTP').click()
    })

})





