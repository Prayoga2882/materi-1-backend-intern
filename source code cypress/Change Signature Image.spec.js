/// <reference types="cypress" />

describe('Change Image Signature', () => {
    it('Change Image Signature', () => {
        cy.visit('https://privy.id')
        cy.contains('Login').click({force: true})
        cy.contains('Log In').should('exist')
        cy.get('[name="user[privyId]"]').type('CG8758',{force: true})
        cy.contains('CONTINUE').click()
        cy.get('[name="user[secret]"]').type('Prayoga123')
        cy.contains('CONTINUE').click()
        cy.contains('Change My Signature Image ').click()
        cy.contains('Add Signature').click({ force: true })
        cy.contains('Image').click()
        cy.contains('Browse').click()

        const filePath = 'TandaTangan.png';
        cy.get('input[name="image"]').attachFile(filePath)
        cy.contains('Crop').click()
        cy.contains('Apply').click()

        cy.get('#__BVID__235 > div > div > div').click()
        const filePath2 = 'TandaTangan.png';
        cy.get('input[name="image"]').attachFile(filePath2)
        cy.contains('Crop').click()
        cy.contains('Apply').click()
        cy.contains('Save').click()
        cy.visit('https://app.privy.id')
    })
})